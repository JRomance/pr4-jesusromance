package clases;

public class Factura {
	private String codigoFactura;
	private String fecha;
	private String nombreProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente uncliente;

	public Factura(String cod) {
		this.codigoFactura = cod;
	}

	public Factura(int cantidad, float precio, String fecha) {

		this.codigoFactura = null;
		this.fecha = fecha;
		this.nombreProducto = "";
		this.precioUnidad = precio;
		this.cantidad = cantidad;
	}

	public Factura() {
		// TODO Auto-generated constructor stub
	}

	public Factura(String codigoFactura, String fecha, String nombreProducto, float precioUnidad, int cantidad,
			Cliente uncliente) {
		super();
		this.codigoFactura = codigoFactura;
		this.fecha = fecha;
		this.nombreProducto = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;
		this.uncliente = uncliente;
	}

	public float calcularPrecioTotal() {

		return (this.cantidad * this.precioUnidad);
	}

	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getUncliente() {
		return uncliente;
	}

	public void setUncliente(Cliente uncliente) {
		this.uncliente = uncliente;
	}
	@Override
	public String toString() {
		return "Factura [codigoFactura=" + codigoFactura + ", fecha=" + fecha + ", nombreProducto=" + nombreProducto
				+ ", precioUnidad=" + precioUnidad + ", cantidad=" + cantidad + ", uncliente=" + uncliente + "]";
	}

}
