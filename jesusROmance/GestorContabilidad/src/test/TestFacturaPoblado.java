package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Factura;
import clases.GestorContabilidad;

class TestFacturaPoblado {
	private static GestorContabilidad gestorPrueba;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestorPrueba = new GestorContabilidad();
		Factura factura1 = new Factura("1234");
		Factura factura2 = new Factura("12");
		Factura factura3 = new Factura("12345");
		gestorPrueba.getListaFacturas().add(factura1);
		gestorPrueba.getListaFacturas().add(factura2);
		gestorPrueba.getListaFacturas().add(factura3);
	}
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	/* Busca factura inexistente */
	@Test
	void testBuscaFacturaInexistente() {

		Factura esperado = gestorPrueba.buscarFactura("12789");
		assertNull(esperado);
	}
	/*insertamos factura nueva y comprobamos que esta*/
	@Test
	public void InsertarFacturaNuevo() {
		Factura actual = new Factura("7895");
		gestorPrueba.altaFactura(actual);
		Factura esperada = gestorPrueba.buscarFactura("7895");
		assertEquals(esperada, actual);
	}
	/*
	 * Buscamos factura existente entre muchas
	 */
	@Test
	public void buscarFacturaExistenteenvarias(){
		Factura actual= new Factura();
		actual.setCodigoFactura("123456");
		
		Factura actual1= new Factura();
		actual1.setCodigoFactura("1234567");
		
		Factura esperada = new Factura();
		gestorPrueba.getListaFacturas().add(actual);
		gestorPrueba.getListaFacturas().add(actual1);
		esperada=gestorPrueba.buscarFactura("1234567");
		
		assertEquals(actual1, esperada);
		
		
	}
	
	/*
	 * Factura mas cara
	 */
	@Test
	public void facturaMasCara(){
		Factura factura= new Factura(5, 1,"1998");
		Factura factura1= new Factura(4, 5,"1998");
		gestorPrueba.getListaFacturas().add(factura);
		gestorPrueba.getListaFacturas().add(factura1);
	Factura esperada= gestorPrueba.facturaMasCara();
	Factura factura3=gestorPrueba.getListaFacturas().get(1);
	assertEquals(factura1, esperada);
	}
	/*
	 * Facturacion de un a�o
	 */
	@Test
	public void FacturaAnual(){
		
		float resultado;
		resultado=gestorPrueba.calcularFacturaAnual("1998");
		float esperado=25;
		assertEquals(esperado, resultado, 0);
		
	}
}
