package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.AfterClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;


class TestClientesVacios {
	static GestorContabilidad gestorPrueba;
	@BeforeAll
	 static void constr() throws Exception {
		gestorPrueba=new GestorContabilidad();
		
	}
	@AfterEach
	void c() {
		gestorPrueba.getListaClientes().clear();
	}
	
	/*a�ade cliente y buscamos que este el cliente con el metodo BuscaCliente si devue*/
	@Test
	void testAltaClienteVacio() {
		Cliente esperado=new Cliente("pepa","123558D",LocalDate.now());
		gestorPrueba.altaCliente(esperado);
		Cliente actual= gestorPrueba.buscarCliente("123558D");
		assertNotNull(actual);
		
	}
	/*Nos devuelve null por que no existe el cliente*/
	@Test
	void testClienteNullMasAntiguo() {
	Cliente cliente=gestorPrueba.clienteMasAntiguo();
	assertNull(cliente);
	}
	



}
