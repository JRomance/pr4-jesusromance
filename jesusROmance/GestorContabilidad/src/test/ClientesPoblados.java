package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.GestorContabilidad;

class ClientesPoblados {
	static GestorContabilidad gestorPrueba;
	/*Generamos gestor y los clientes y los A�adimos a ListaClientes*/
	@BeforeAll
	static void constr() throws Exception { 
		gestorPrueba=new GestorContabilidad();
		Cliente cliente1 =new Cliente("Jesus","12345R",LocalDate.parse("2002-03-15"));
		Cliente cliente2 =new Cliente("Pepe","R1478",LocalDate.parse("2008-07-15"));
		Cliente cliente3 =new Cliente("Marisa","6587A",LocalDate.parse("2009-08-25"));
		gestorPrueba.getListaClientes().add(cliente1);
		gestorPrueba.getListaClientes().add(cliente2);
		gestorPrueba.getListaClientes().add(cliente3);
	}
	@AfterAll
	static void der() {
		gestorPrueba.getListaClientes().clear();
	}
	/*
	 * Buscar con Dni existente
	 * */
	@Test
	void testBuscaClienteExistente() {
		String dni="12345R";
		Cliente actual=gestorPrueba.buscarCliente(dni);
		assertNotNull(actual);
	}
	/*
	 * Buscar con dni Nulo
	 * */
	@Test
	void TestClienteDniInexistente() {
		String dni="8975";
		Cliente actual=gestorPrueba.buscarCliente(dni);
		assertNull(actual);
	}
	/*alta cliente ArrayList poblado con dni repetido*/

	@Test
	void testAltaClienteRepetido() {
		Cliente esperado=new Cliente("manoli","12345R",LocalDate.now());
		gestorPrueba.altaCliente(esperado);
		Cliente actual= gestorPrueba.buscarCliente("12345R");
		assertNotEquals(esperado, actual);
	}
	/*Inserta cliente en lista poblada*/
	@Test
	void testAltaClientenuevo() {
		Cliente esperado=new Cliente("pepa","1235R",LocalDate.now());
		gestorPrueba.altaCliente(esperado);
		Cliente actual= gestorPrueba.buscarCliente("1235R");
		assertEquals(esperado, actual);
	}
	/*que exista un cliente y no sea nulo  */
	@Test
	void testClienteMasAntiguo() {
		Cliente cliente=gestorPrueba.clienteMasAntiguo();
		assertNotNull(cliente);
	}
	/*Buscamos cliente mas antiguo*/
	@Test
	void testClienteMasAntiguoEnLista() {
		Cliente cliente=gestorPrueba.clienteMasAntiguo();
	
			Cliente cliente1=gestorPrueba.getListaClientes().get(0);
		assertEquals(cliente, cliente1);
	
	}
}
